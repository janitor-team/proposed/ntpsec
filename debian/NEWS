ntpsec (1.2.0+dfsg1-1) unstable; urgency=medium

  IANA has assigned port 4460 to NTS-KE.  ntpd now uses port 4460 in both
  client and server modes.

 -- Richard Laager <rlaager@debian.org>  Mon, 16 Nov 2020 23:32:31 -0600

ntpsec (1.1.9+dfsg1-1) unstable; urgency=medium

  IANA has assigned port 4460 to NTS-KE.  ntpd in server mode now listens
  on both ports 123 and 4460 to facilitate transitioning.  ntpd in client
  mode now defaults to port 4460.  In the next upstream release, ntpd will
  listen only on port 4460.

 -- Richard Laager <rlaager@debian.org>  Wed, 12 Aug 2020 23:57:16 -0500

ntpsec (1.1.8+dfsg1-4) unstable; urgency=medium

  The NTS label has been changed.  This is a backwards incompatible change!
  This affects NTS in both client and server modes.  Public servers (e.g.
  CloudFlare) have already changed.

  The AppArmor policy no longer imports abstractions/ssl_certs and ssl_keys.
  ntpd needs to be able to read the certificate and key after dropping
  privileges; otherwise, it cannot handle certificate renewals (without a full
  restart).  Use a deploy script to copy the certificate and keys to
  /etc/ntpsec/cert-chain.pem and key.pem and permission them appropriately
  (e.g. mode 640 and group ntpsec).  For certbot users, a deploy script is
  provided; set the appropriate certificate name (e.g. the system's hostname)
  in the NTPSEC_CERTBOT_CERT_NAME variable in /etc/default/ntpsec.

 -- Richard Laager <rlaager@wiktel.com>  Sat, 25 Apr 2020 13:35:14 -0500

ntpsec (1.1.1+dfsg1-1) unstable; urgency=medium

  To address various bugs, a number of things have been renamed to avoid
  conflicting with the ntp packages.  Most critically, /etc/ntp.conf is now
  at /etc/ntpsec/ntp.conf.  Also, the ntp service has been renamed to ntpsec.
  For systemd, ntp.service is now ntpsec.service.  For sysvinit,
  /etc/init.d/ntp is now /etc/init.d/ntpsec.  This is not an exhaustive list.

 -- Richard Laager <rlaager@wiktel.com>  Tue, 31 Jul 2018 03:18:23 -0500
